[ ![Download](https://api.bintray.com/packages/marshallpierce/maven/org.mpierce.ktor.session%3Aktor-session-gcp-firestore/images/download.svg) ](https://bintray.com/marshallpierce/maven/org.mpierce.ktor.session%3Aktor-session-gcp-firestore/_latestVersion)

This is an implementation of ktor session storage backed by Google Firestore.

# Limitations

Firestore doesn't support expiration, so each document gets a timestamp embedded in it that is then checked when the document is read. Expired documents will accumulate over time, and this library makes no attempt to clean them up.

# Running the tests

See https://cloud.google.com/firestore/docs/quickstart to create a GCP project with Firestore.

Create a GCP Project and a Service Account with suitable privileges in it. Download the account's credentials JSON. The test currently expects to find the credentials at `tmp/ktor-session-firestore.json`. (Or, use whatever names you want, and tweak the source.)
