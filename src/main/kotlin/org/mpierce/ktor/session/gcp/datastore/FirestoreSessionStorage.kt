package org.mpierce.ktor.session.gcp.datastore

import com.google.api.core.ApiFuture
import com.google.cloud.Timestamp
import com.google.cloud.firestore.Blob
import com.google.cloud.firestore.Firestore
import com.google.common.util.concurrent.ListenableFuture
import io.ktor.sessions.SessionStorage
import kotlinx.coroutines.experimental.future.await
import kotlinx.coroutines.experimental.io.ByteReadChannel
import kotlinx.coroutines.experimental.io.ByteWriteChannel
import kotlinx.coroutines.experimental.io.readAvailable
import kotlinx.coroutines.experimental.io.reader
import net.javacrumbs.futureconverter.java8guava.FutureConverter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.ByteArrayOutputStream
import java.time.Duration
import java.time.Instant
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit
import kotlin.coroutines.experimental.coroutineContext

const val DATA_FIELD = "contents"
const val EXPIRATION_FIELD = "exp"

/**
 * Session storage in GCP Firestore.
 *
 * Because Firestore imposes a max document limit of 1 MiB - 4 bytes, don't store too much in your session.
 */
class FirestoreSessionStorage(private val firestore: Firestore,
                              private val ttl: Duration,
                              private val collection: String) : SimplifiedSessionStorage() {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(FirestoreSessionStorage::class.java)
    }

    override suspend fun read(id: String): ByteArray? {
        val future =
                firestore.runTransaction { txn ->
                    logger.trace("Read tx running")
                    val docRef = docRef(id)
                    val snapshot = txn.get(docRef).get()

                    if (!snapshot.exists()) {
                        logger.debug("id $id not found")
                        null
                    } else {
                        val exp = snapshot.getTimestamp(EXPIRATION_FIELD)
                        val blob = snapshot.getBlob(DATA_FIELD)
                        if (blob == null || exp == null) {
                            logger.warn("id $id: found document, but required fields are missing")
                            // corrupt data?
                            null
                        } else {
                            val now = Instant.now()
                            val nowTimestamp = Timestamp.ofTimeSecondsAndNanos(now.epochSecond, now.nano)

                            if (nowTimestamp <= exp) {
                                val newExp = expirationTimestamp()
                                logger.trace("id $id not expired with exp $exp, updating exp to $newExp")
                                txn.update(docRef, mapOf(EXPIRATION_FIELD to newExp))

                                blob.toBytes()
                            } else {
                                logger.debug("id $id expired")
                                txn.delete(docRef)
                                null
                            }
                        }
                    }
                }

        return future.asCompletableFuture().await()
    }

    override suspend fun write(id: String, data: ByteArray?) {
        val docRef = docRef(id)

        val future = if (data == null) {
            docRef.delete()
        } else {
            val expirationTimestamp = expirationTimestamp()
            logger.trace("id $id write with exp $expirationTimestamp")
            docRef.set(mapOf(
                    DATA_FIELD to Blob.fromBytes(data),
                    EXPIRATION_FIELD to expirationTimestamp))
        }

        future.asCompletableFuture().await()
    }

    private fun docRef(id: String) = firestore.collection(collection).document(id)

    private fun expirationTimestamp(): Timestamp {
        val exp = Instant.now().plus(ttl)
        return Timestamp.ofTimeSecondsAndNanos(exp.epochSecond, exp.nano)
    }
}

fun <T> ApiFuture<T>.asCompletableFuture(): CompletableFuture<T> {
    // turn it into a Guava ListenableFuture, so we can re-use a library that converts it to a CompletableFuture
    return FutureConverter.toCompletableFuture(ListenableFutureAdapter(this))
}

/**
 * ApiFuture is just a renamed ListenableFuture, so straightforward delegation is all that's needed.
 */
class ListenableFutureAdapter<T>(private val apiFuture: ApiFuture<T>) : ListenableFuture<T> {
    override fun addListener(listener: Runnable?, executor: Executor?) = apiFuture.addListener(listener, executor)

    override fun isDone(): Boolean = apiFuture.isDone

    override fun get(): T = apiFuture.get()

    override fun get(timeout: Long, unit: TimeUnit?): T = apiFuture.get(timeout, unit)

    override fun cancel(mayInterruptIfRunning: Boolean): Boolean = apiFuture.cancel(mayInterruptIfRunning)

    override fun isCancelled(): Boolean = apiFuture.isCancelled
}

/**
 * Since we don't have an obvious place to hang on to re-usable buffers, use this helper from
 * https://ktor.io/features/sessions.html. It does allocate, but oh well...
 */
abstract class SimplifiedSessionStorage : SessionStorage {
    abstract suspend fun read(id: String): ByteArray?
    abstract suspend fun write(id: String, data: ByteArray?)

    override suspend fun invalidate(id: String) {
        write(id, null)
    }

    override suspend fun <R> read(id: String, consumer: suspend (ByteReadChannel) -> R): R {
        val data = read(id) ?: throw NoSuchElementException("Session $id not found")
        return consumer(ByteReadChannel(data))
    }

    override suspend fun write(id: String, provider: suspend (ByteWriteChannel) -> Unit) {
        return provider(reader(coroutineContext, autoFlush = true) {
            write(id, channel.readAvailable())
        }.channel)
    }
}

suspend fun ByteReadChannel.readAvailable(): ByteArray {
    val data = ByteArrayOutputStream()
    val temp = ByteArray(1024)
    while (!isClosedForRead) {
        val read = readAvailable(temp)
        if (read <= 0) break
        data.write(temp, 0, read)
    }
    return data.toByteArray()
}
