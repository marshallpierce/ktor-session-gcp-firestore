package org.mpierce.ktor.session.gcp.datastore

import com.google.api.gax.core.FixedCredentialsProvider
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.FirestoreOptions
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.http.HttpMethod
import io.ktor.network.util.ioCoroutineDispatcher
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import io.ktor.sessions.Sessions
import io.ktor.sessions.get
import io.ktor.sessions.header
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.time.delay
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.nio.file.Files
import java.nio.file.Paths
import java.time.Duration
import java.util.UUID
import java.util.concurrent.ThreadLocalRandom


internal class FirestoreSessionStorageTest {

    private lateinit var random: ThreadLocalRandom

    private val collection = "sessions"

    @BeforeEach
    internal fun setUp() {
        random = ThreadLocalRandom.current()
    }

    @AfterEach
    internal fun tearDown() {
        withFirestore { f ->
            while (true) {
                val snapshot = f.collection(collection)
                        .limit(500)
                        .get()
                        .get()
                if (snapshot.isEmpty) {
                    break
                }

                val batch = f.batch()

                snapshot.documents.forEach {
                    batch.delete(f.collection(collection).document(it.id))
                }

                batch.commit().get()
            }
        }
    }

    @Test
    internal fun readThenWrite() {
        withFirestore { f ->
            val storage = FirestoreSessionStorage(f, Duration.ofMinutes(1), collection)

            val id = UUID.randomUUID()
            val bytes = ByteArray(100)
            random.nextBytes(bytes)
            storage.write(id.toString(), bytes)

            assertArrayEquals(bytes, storage.read(id.toString()))
        }
    }

    @Test
    internal fun readMissingReturnsNull() {
        withFirestore { f ->
            val storage = FirestoreSessionStorage(f, Duration.ofMinutes(1), collection)

            assertNull(storage.read(UUID.randomUUID().toString()))
        }
    }

    @Test
    fun readUpdatesTtl() {
        withFirestore { f ->
            val storage = FirestoreSessionStorage(f, Duration.ofMillis(4000), collection)

            val id = UUID.randomUUID()
            val bytes = ByteArray(100)
            random.nextBytes(bytes)
            storage.write(id.toString(), bytes)

            assertArrayEquals(bytes, storage.read(id.toString()))

            for (i in 0..10) {
                delay(Duration.ofMillis(500))
                storage.read(id.toString())
            }

            // would have expired by now if not refreshed by the above reads

            assertArrayEquals(bytes, storage.read(id.toString()))
        }
    }

    @Test
    internal fun writeExpiresAtTtl() {
        withFirestore { f ->
            val storage = FirestoreSessionStorage(f, Duration.ofMillis(4000), collection)

            val id = UUID.randomUUID()
            val bytes = ByteArray(100)
            random.nextBytes(bytes)
            storage.write(id.toString(), bytes)

            assertArrayEquals(bytes, storage.read(id.toString()))

            delay(Duration.ofMillis(5000))

            assertNull(storage.read(id.toString()))
        }
    }

    @Test
    internal fun writeNullDeletes() {
        withFirestore { f ->
            val storage = FirestoreSessionStorage(f, Duration.ofMinutes(1), collection)

            val id = UUID.randomUUID()
            val bytes = ByteArray(100)
            random.nextBytes(bytes)
            storage.write(id.toString(), bytes)

            assertArrayEquals(bytes, storage.read(id.toString()))

            storage.write(id.toString(), null)

            assertNull(storage.read(id.toString()))
        }
    }

    @Test
    internal fun manyReadThenWrites() {
        withFirestore { f ->
            val storage = FirestoreSessionStorage(f, Duration.ofMinutes(1), collection)

            val jobs = List(100) {
                async(ioCoroutineDispatcher) {
                    val id = UUID.randomUUID()
                    val bytes = ByteArray(100)
                    random.nextBytes(bytes)
                    storage.write(id.toString(), bytes)

                    assertArrayEquals(bytes, storage.read(id.toString()))
                }
            }

            jobs.forEach { it.await() }
        }
    }

    @Test
    fun testIntegration() {
        class Visitcounter(val visits: Int)

        withFirestore { f ->
            withTestApplication {
                application.apply {
                    install(Sessions) {
                        header<Visitcounter>("session", FirestoreSessionStorage(f, Duration.ofMinutes(1), collection))
                    }
                    routing {
                        get("/") {
                            val session = call.sessions.get<Visitcounter>()
                            val nvisits = session?.visits ?: 0
                            call.sessions.set(Visitcounter(nvisits + 1))
                            call.respondText("Visits $nvisits")
                        }
                    }
                }

                val r1 = handleRequest(HttpMethod.Get, "/").response
                assertEquals("Visits 0", r1.content)
                val sessionHeader = r1.headers["session"]!!
                assertEquals("Visits 1", handleRequest(HttpMethod.Get, "/") {
                    addHeader("session", sessionHeader)
                }.response.content)
            }
        }

    }
}

fun withFirestore(block: suspend (Firestore) -> Unit) {
    runBlocking {
        Files.newInputStream(Paths.get("tmp/ktor-session-firestore.json")).use { stream ->
            val credentials = ServiceAccountCredentials.fromStream(stream)
            val firestore = FirestoreOptions.getDefaultInstance().toBuilder()
                    // in 0.57, neither setCredentialsProvider nor setCredentials correctly determines the project id,
                    // so we have to set it. Also, setCredentials doesn't appear to work at all.
                    .setProjectId("ktor-session-firestore-2")
                    .setTimestampsInSnapshotsEnabled(true)
//                    .setCredentials(credentials)
                    .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
                    .build()
                    .service

            firestore.use {
                block(it)
            }
        }

    }
}
